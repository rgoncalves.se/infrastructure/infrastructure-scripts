#!/bin/bash
#
# Functions for ipmi scripts and tools
# NOTE :
# - All environment variables and command are
#   made to be used with Dell IDRAC6 boards on
#   latest firmware update.

# @ipmi_load_environment
# Load ipmi environment variables,
# on failure set default.
function ipmi_load_environment() {
	: ${IPMI_IP:="192.168.5.100"}
	: ${IPMI_USER:="root"}
	: ${IPMI_PASS:="calvin"}
	log_important "IPMI_IP is ${IPMI_IP}"
	log_important "IPMI_USER is ${IPMI_USER}"
	log_important "IPMI_PASS is ${IPMI_PASS}"
}

# @ipmi_connection
# Connect to a remote host via IPMI protocol
# - 1 : remote command to execute
# - IPMI_IP : IP address of the IPMI interface from remote host
# - IPMI_USER : IPMI user for executing remote command (default -> root)
# - IPMI_PASS : user's password for remote login (default -> calvin)
function ipmi_connection() {
	ipmitool -I lanplus -H ${IPMI_IP} -U ${IPMI_USER} -P ${IPMI_PASS} ${@}
}
