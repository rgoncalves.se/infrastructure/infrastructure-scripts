## functions

### usage

Display help

### ipmi_load_environment

Load ipmi environment variables,
on failure set default.

### ipmi_connection

Connect to a remote host via IPMI protocol

- 1 : remote command to execute

- IPMI_IP : IP address of the IPMI interface from remote host

- IPMI_USER : IPMI user for executing remote command (default -> root)

- IPMI_PASS : user's password for remote login (default -> calvin)
