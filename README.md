## structure

Each subdirectory corresponds to a tool/set of tools.
Each one is made of one or many executables bash scripts.
Although, all functions have to exists in a file called `functions.sh`, 
allowing basic programmed behaviors to be loaded externally.

``` c
.
├── gen2
├── git
├── ipmi
├── LICENSE
├── log
├── README.md
├── readmes
├── util
└── wireguard

7 directories, 2 files
```

## functions

For each new function, a commment block has to be placed above it
(technically not required, but easier for editing).
This allows us to generate per-subdirectory documentation,
through : `bash readmes/generate_readme`.

## tools

### [gen2](gen2)

> `bash gen2/generate_templates`

Originally a fully declarative python module, it has been turned into a modular bash script.
This tool make automated jinja2 templating possible, from a yaml defined network infrastructure.
Each templates found in `gen2/templates/` has to use pre-processing variables at its beginning.

- TEMPLATE_OUTPUT : output file for generated template
- TEMPLATE_TYPE : `all` or `per-type`
- TEMPLATE_SUFFIX : suffix and/or filetype, if TEMPLATE_TYPE is `per-type`.

The original implementation in python is still available in its own repository
[:/rgoncalves.se/infrastructure/infrastructure-generation](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation)

### [git](git)

> `bash git/sync_description_internal`

Uses *curl*, *cut* and *sed* for webscrapping each public infrastructure git repository on gitlab,
and then update description in internal repositories.

> `bash git/init_public_remotes`

Given a list of directories (git repositories),
initializes the remote `public` with the right naming convention for git url.

> `bash git/sync_repository`

Given a list of directories (git repositories),
synchronize (pull then push) with all remotes.

### [guests](guests)

Scripts to be run on guest machines and servers.

> `sh guests/motd`

Generates and output motd on standard output.

![](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-scripts/-/raw/master/.examples/motd_01.jpg)

![](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-scripts/-/raw/master/.examples/motd_02.jpg)

### [readmes](readmes)

> `bash readmes/generate_readmes`

Automatically creates per-subdirectory documentation via README file in markdown.
It uses all human readables files under a subdirectory, and parses specific statements, such as :

``` bash
∣ # @this_is_a_beautiful_function
∣ # This function allow us to make beautiful things,
∣ # so much awesome things that I don't know what to say about it.
∣ function this_is_a_beautiful_function() {
∣ 	figlet -f slant "$(uname -r)" | lolcat
∣ }
```

The output looks like this :

``` markdown
∣ ### this_is_a_beautiful_function
∣ This function allow us to make beautiful things,
∣ so much awesome things that I don't know what to say about it.
```

### [wireguard](wireguard)

> `bash wireguard/generate_keys`

Set of scripts and functions, generating private and public keypair for each hosts.
Using pre-generated configuration files from `gen2/generate_templates`,
it parses each files and uses *sed* and `%PATTERN%` for filling configurations with pub/private keys and internet addresses.

### [util](util)

External scripts that does (should) not depend on any other external scripts.

Contrary to the other sub-directory, there is not one `functions.sh` files containing all related functions to the main task of the subdirectory.

> `bash util/log_test`

Show all possible logging functions for stdout/stderr.
