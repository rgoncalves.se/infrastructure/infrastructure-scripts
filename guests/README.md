## scripts

Scripts to be run on guest machines and servers.

> `sh guests/motd`

Generates and output motd on standard output.

![](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-scripts/-/raw/master/.examples/motd_01.jpg)

![](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-scripts/-/raw/master/.examples/motd_02.jpg)

## functions

### show_line

Show full motd line with prefix

### show_label

Show a line with key/value(s)

- 1 : line description

- 2 : value

### show_progress

Show a line with key/progress bar

- 1 : line description

- 2 : used value

- 3 : maximum value
