#!/bin/bash
#
# Functions for Wireguard scripts

# @wg_generate
# Generate public key AND private key for wireguard configurations
# - ${1} : public key destination
# - ${2} : private key destination
# (DOES NOT generate new file(s) if already exist !)
function wg_generate() {
	if [ ! -f ${1} ] || [ ! -f ${2} ]; then
		umask 077
		wg genkey | tee ${2} | wg pubkey > ${1}
		log_state "generated key with public key $(cat ${1})"
	else
		log_warning "wg keys already exist, skipping"
	fi
}
