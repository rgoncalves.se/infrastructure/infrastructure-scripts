## scripts

> `bash wireguard/generate_keys`

Set of scripts and functions, generating private and public keypair for each hosts.
Using pre-generated configuration files from `gen2/generate_templates`,
it parses each files and uses *sed* and `%PATTERN%` for filling configurations with pub/private keys and internet addresses.

## functions

### wg_generate

Generate public key AND private key for wireguard configurations

- ${1} : public key destination

- ${2} : private key destination
(DOES NOT generate new file(s) if already exist !)

### usage

Show usage for generating wireguard key
