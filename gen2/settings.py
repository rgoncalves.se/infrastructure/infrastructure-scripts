#!/usr/bin/env python3
#
# Custom settings file for j2cli

from jinja2 import Undefined

class SilentUndefined(Undefined):
	""" Return None if a variable is undefined in a jinja2 template. """
	def _fail_with_undefined_error(self, *args, **kwargs):
		return None

def j2_environment_params():
	""" Extra parameters for the Jinja2 Environment. """
	return dict(
		trim_blocks=True,
		lstrip_blocks=True,
		undefined=SilentUndefined
	)

