#!/bin/bash
#
# Gen2 functions for gen2 scripts


# @gen2_read_options
# Read options in template, for pre-processing
# - ${1} : template file
#
# SUPPORTED OPTIONS:
# - TEMPLATE_OUTPUT : output file for generated config
# - TEMPLATE_TYPE : (all | servers | clients | components
#                    per-all | per-servers | per-clients | per-components)
function gen2_read_options() {
	# Read line by line
	while IFS= read -r line; do
		# If correct line pattern, read values
		if echo "${line}" | grep "^##" >/dev/null; then
			line=$(echo ${line} | sed 's/## //g')
			log_trace "reading ${line}"
			gen2_load_option "${line}"
		# When option block is gone, exit the function
		else
			return 0
		fi
	done < ${1}
}

# @gen2_load_option
# Load an option variable, read from a template,
# ONLY on a valid input
# - ${1} : (in)valid key+value
# - ${AVAILABLE_OPTIONS} : list containing available options/variables that can
#   be contained in the jinja template
function gen2_load_option() {
	OPTION_KEY=$(echo "${1}" | cut -d"=" -f1)
	for valid_option in ${AVAILABLE_OPTIONS[@]}; do
		if [ "${valid_option}" = "${OPTION_KEY}" ]; then
			log_success "${OPTION_KEY} is a valid option. Loading it."
			export "${1}"
			return 0
		fi
	done
	log_error "${OPTION_KEY} is not a valid option. NOT loading it."
	return 1
}

# @gen2_gen_configuration
# Generate configuration file from template,
# then export it
# - ${1} : template file
# - ${2} : data file
# - ${3} : output file for generated configuration
function gen2_gen_configuration() {
	j2 --customize $(rsource_path "gen2/settings.py") \
		-f yaml "${1}" "${2}" \
		> $(eval echo "$3")
}

# @gen2_gen_per_host_yaml
# Parse yaml file per host
# - ${1} : data file
# - ${HOST_FILE_SUFFIX} : suffix for host file
# - ${HOST_FILE_DIR} : directory for storing generated host files
function gen2_gen_per_host_yaml() {
	MINIMAL_YAML=$(cat "${1}" | grep -v "^$" | grep -v "^#")
	HOST_NAME=""
	HOST_FILE=""
	while IFS= read -r line; do
		if [ "${line:0:3}" = "  -" ]; then
			HOST_NAME=$(echo "${line}" | sed 's/- name://g; s/"//g; s/ //g')
			HOST_FILE="${HOST_FILE_DIR}/${HOST_NAME}${HOST_FILE_SUFFIX}"
			log_heading "parsing ${HOST_NAME}"
			echo "${line}" | sed 's/^....//g' > ${HOST_FILE}
		fi
		if [ "${line:0:3}" = "   " ]; then
			echo "${line}" | sed 's/^....//g' >> ${HOST_FILE}
		fi
	done <<< "${MINIMAL_YAML}"
}
