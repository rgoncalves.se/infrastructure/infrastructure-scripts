## scripts

> `bash gen2/generate_templates`

Originally a fully declarative python module, it has been turned into a modular bash script.
This tool make automated jinja2 templating possible, from a yaml defined network infrastructure.
Each templates found in `gen2/templates/` has to use pre-processing variables at its beginning.

- TEMPLATE_OUTPUT : output file for generated template
- TEMPLATE_TYPE : `all` or `per-type`
- TEMPLATE_SUFFIX : suffix and/or filetype, if TEMPLATE_TYPE is `per-type`.

The original implementation in python is still available in its own repository
[:/rgoncalves.se/infrastructure/infrastructure-generation](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation)

## functions

### usage

Display help

### check_arguments

Check arguments validity

- ${YAML_DATA_FILE} : must be a valid file

### check_options

Check options validity

### gen2_read_options

Read options in template, for pre-processing

- ${1} : template file

### gen2_load_option

Load an option variable, read from a template,
ONLY on a valid input

- ${1} : (in)valid key+value

- ${AVAILABLE_OPTIONS} : list containing available options/variables that can
  be contained in the jinja template

### gen2_gen_configuration

Generate configuration file from template,
then export it

- ${1} : template file

- ${2} : data file

- ${3} : output file for generated configuration

### gen2_gen_per_host_yaml

Parse yaml file per host

- ${1} : data file

- ${HOST_FILE_SUFFIX} : suffix for host file

- ${HOST_FILE_DIR} : directory for storing generated host files
