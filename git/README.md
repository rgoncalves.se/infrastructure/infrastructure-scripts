## scripts

> `bash git/sync_description_internal`

Uses *curl*, *cut* and *sed* for webscrapping each public infrastructure git repository on gitlab,
and then update description in internal repositories.

> `bash git/init_public_remotes`

Given a list of directories (git repositories),
initializes the remote `public` with the right naming convention for git url.

> `bash git/sync_repository`

Given a list of directories (git repositories),
synchronize (pull then push) with all remotes.

## functions

### usage

Display help

### git_pushall

push all changes to all remote
(for infrastructure-scripts we use :

- master -> as internal

- public -> as public (gitlab)

### git_commit_new

add and commit new files under a same commit 
use this carrefully and only for automated sync/update in scripts

### git_commit_updated

add and commit updated files under a same commit
use this carrefully and only for automated sync/update in scripts
