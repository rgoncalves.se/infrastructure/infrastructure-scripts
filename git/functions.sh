#!/bin/bash

# git functions bash file
# consider that file without external depencies

# git date pattern used for new/update files in commit messages
GIT_DATE_PATTERN="+%Y-%m-%d"
GIT_DATE=$(date ${GIT_DATE_PATTERN})

# @git_pushall
# push all changes to all remote
# (for infrastructure-scripts we use :
# - master -> as internal
# - public -> as public (gitlab)
function git_pushall() {
	git remote | xargs -L1 git push --all
}

# @git_commit_new
# add and commit new files under a same commit 
# use this carrefully and only for automated sync/update in scripts
function git_commit_new() {
	git add $(git ls-files -o --exclude-standard)
	git commit -m "${GIT_DATE} : Added"
}

# @git_commit_updated
# add and commit updated files under a same commit
# use this carrefully and only for automated sync/update in scripts
function git_commit_updated() {
	git add -u
	git commit -m "${GIT_DATE} : Updated"
}
