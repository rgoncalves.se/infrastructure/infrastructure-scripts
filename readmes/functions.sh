#!/bin/bash
#
# Readmes functions

# @readmes_parse_file
# Parse a human readable file.
# Then generate markdown files with functions usage.
# - ${1} : input file
# - ${2} : output file (markdown readme)
function readmes_parse_file() {
	local is_block
	is_block=false
	while IFS= read -r line; do
		# Check if line is a beginning of a block function.
		if [ "${line:0:3}" = "# @" ]; then
			is_block=true
			readmes_append_line "${line}" "${2}"
			continue
		fi
		# Check if line is part of a block function.
		if [ "${is_block}" = true ] && [ "${line:0:2}" = "# " ]; then
			readmes_append_line "${line}" "${2}"
			continue
		fi
		is_block=false
	done < "${1}"
}

# @readmes_append_master_readme
# Parse master git README.md file,
# and return the description of the specified subdirectory
# - 1 : pattern
# - 2 : input file
# - 3 : output file
function readmes_append_master_readme() {
	local is_block
	local master_line
	is_block=false
	while IFS= read -r master_line; do
		if [ "${master_line:0:3}" = "###" ]; then
			if [ "${master_line:4}" = "[${1}](${1})" ]; then
				log_warning "appending master readme section ${1}"
				is_block=true
				master_line="## scripts"
			else
				is_block=false
			fi
		fi
		if "${is_block}"; then
			echo "${master_line}" >> "${3}"
			continue
		fi
	done < "${2}"
	echo "## functions" >> "${3}"
}

# @readmes_append_line
# Append line to a file, removing pre-processing blocks.
# And convert first line with mardown title.
# - ${1} : input line
# - ${2} : output file
function readmes_append_line() {
	line=$(sed "s/# //g" <<< "${1}")
	# Process third level mardkdown title
	if [ "${line:0:1}" = "@" ]; then
		line="\n### ${line:1}\n"
	# Process lists
	elif [ "${line:0:1}" = "-" ]; then
		line="\n${line}"
	fi
	printf "${line}\n" >> "${2}"
}
