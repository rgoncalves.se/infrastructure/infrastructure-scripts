## scripts

> `bash readmes/generate_readmes`

Automatically creates per-subdirectory documentation via README file in markdown.
It uses all human readables files under a subdirectory, and parses specific statements, such as :

``` bash
∣ # @this_is_a_beautiful_function
∣ # This function allow us to make beautiful things,
∣ # so much awesome things that I don't know what to say about it.
∣ function this_is_a_beautiful_function() {
∣ 	figlet -f slant "$(uname -r)" | lolcat
∣ }
```

The output looks like this :

``` markdown
∣ ### this_is_a_beautiful_function
∣ This function allow us to make beautiful things,
∣ so much awesome things that I don't know what to say about it.
```

## functions

### readmes_parse_file

Parse a human readable file.
Then generate markdown files with functions usage.

- ${1} : input file

- ${2} : output file (markdown readme)

### readmes_append_master_readme

Parse master git README.md file,
and return the description of the specified subdirectory

- 1 : pattern

- 2 : input file

- 3 : output file

### readmes_append_line

Append line to a file, removing pre-processing blocks.
And convert first line with mardown title.

- ${1} : input line

- ${2} : output file
