#!/bin/bash
#
# Exit condition

# @exit_var_empty
# Exit on empty variable,
# and show usage
function exit_var_empty() {
	if [ -z "${1}" ]; then
		log_error "Please fill all required variables"
		usage
		exit 1
	fi
}

