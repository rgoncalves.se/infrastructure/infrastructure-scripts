## scripts

External scripts that does (should) not depend on any other external scripts.

Contrary to the other sub-directory, there is not one `functions.sh` files containing all related functions to the main task of the subdirectory.

> `bash util/log_test`

Show all possible logging functions for stdout/stderr.
## functions

### rsource_path

Return the relative path of a file,
according to this git repository

### rsource

Source the a library from this git repository

### log_pattern

generate a logging line according to parameters:

- foreground color

- background color

- logging title

- logging data

### log_get_datefile

return the data at time from the main process
this allow use to pack all logging data from one script
into one same file

### log_get_date

return the current date and time (with seconds),
essentially used for line logging.

### log_main

main function to be called when creating a new logging shortcut
this one allows use to save the logging output into one file,
allowing it to be read later.

### log_CALLABLE_FUNCTIONS

call these functions below in your scripts

### log_state

log a statement and flip between SUCCESS and ERROR depending on the result from the previous command.
