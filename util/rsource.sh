#!/bin/bash

# @rsource_path
# Return the relative path of a file,
# according to this git repository
function rsource_path() {
	echo $(realpath "$(dirname ${BASH_SOURCE[0]})/../${1}")
}


# @rsource
# Source the a library from this git repository
function rsource() {
	source "$(rsource_path ${1})"
}

