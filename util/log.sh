#!/bin/bash

LOG_DIR="log"

source "$(dirname ${BASH_SOURCE[0]})/../util/rsource.sh"

# @log_pattern
# generate a logging line according to parameters:
# - foreground color
# - background color
# - logging title
# - logging data
function log_pattern() {
	# style
	tput setab ${1}
	tput setaf ${2}
	tput bold
	# message
	echo -en "$(log_get_date)  "
	echo -en "${3}"
	# clear
	tput op
	tput sgr0
	echo -e " :: ${4}"
}

# @log_get_datefile
# return the data at time from the main process
# this allow use to pack all logging data from one script
# into one same file
function log_get_datefile() {
	PARENT=$(ps -eo pid,lstart | grep ${$} | grep -v "grep")
	DATE=$(echo ${PARENT} | sed "s/[^ ]* //")
	date -d "$DATE" +%Y-%m-%d_%H:%M:%S
}

# @log_get_date
# return the current date and time (with seconds),
# essentially used for line logging.
function log_get_date() {
	date "+%Y-%m-%d %H:%M:%S"
}


# @log_main
# main function to be called when creating a new logging shortcut
# this one allows use to save the logging output into one file,
# allowing it to be read later.
function log_main() {
	# Log is enabled by default
	if [ -z "${LOG_DISABLE}" ]; then
		log_pattern "${@}" | tee -a $(rsource_path ${LOG_DIR})/$(log_get_datefile).log
	fi
}

# @log_CALLABLE_FUNCTIONS
# call these functions below in your scripts
function log() {
	log_main 0  248 "LOG       ..." "${1}"
}
function log_trace() {
	log_main 0  242 "TRACE     ..." "${1}"
}
function log_success() {
	log_main 0 70   "SUCCESS   ..." "${1}"
}
function log_important() {
	log_main 220 232  "IMPORTANT ..." "${1}"
}
function log_warning() {
	log_main 208 232  "WARNING   ..." "${1}"
}
function log_heading() {
	log_main 32 232   "HEADING   ..." "${1}"
}
function log_error() {
	log_main 0 196  "ERROR     ..." "${1}"
}
function log_critical() {
	log_main 0 196  "CRITICAL  ..." "${1}"
}
function log_alert() {
	log_main 196 232  "ALERT     ..." "${1}"
}
function log_emergency() {
	log_main 196 255  "EMERGENCY ..." "${1}"
}
# @log_state
# log a statement and flip between SUCCESS and ERROR depending on the result from the previous command.
function log_state() {
	if [ ${?} -eq 0 ]; then
		log_success "${1}"
		true
	else
		log_error "${1}"
		false
	fi
}

# log library logging it's own loading
log_trace "loaded log library from ${BASH_SOURCE[0]}"
